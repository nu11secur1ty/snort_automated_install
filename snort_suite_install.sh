#!/bin/bash                                                                                                 
#                                                                                                                                                                                                                                                          #
# Author: sickness                                                                                                                                                                                                                                         #
# Date: 06-01-2015                                                                                                                                                                                                                                         #
#                                                                                                                                                                                                                                                          #
# Resources: https://snort.org/documents                                                                                                                                                                                                                   #
#            https://www.snorby.org/#installation                                                                                                                                                                                                          #
#            https://www.phusionpassenger.com/documentation/Users%20guide%20Apache.html#install_on_debian_ubuntu                                                                                                                                           #
#                                                                                                                                                                                                                                                          #                        
# Info: Automated Snort, Barnyard2, Snorby installataion and configuration script for Kali Linux.                                                                                                                                                          #
# Disclaimer: I tested this on Kali Linux, if you wish to use this script on a different distribution you might have to edit it.                                                                                                                           #
#                                                                                                                                                                                                                                                          #
# Additional notes:                                                                                                                                                                                                                                        #
#                   - Colours shamelessly stolen from g0tmi1ks kali script: https://github.com/g0tmi1k/os-scripts/blob/master/kali.sh                                                                                                                      #
#                   - I have not tested this but you might be able to modify my script to work with Suricata: https://www.corelan.be/index.php/2011/02/27/cheat-sheet-installing-snorby-2-2-with-apache2-and-suricata-with-barnyard2-on-ubuntu-10-x/       #
#                                                                                                                                                                                                                                                          #
############################################################################################################################################################################################################################################################

#--- Define some variables 
debug="off"                                                 # If set to "on" will display all the output of each command
subnet="172.16.99.0\/24"                                    # Used in snort configuration
interface="eth0"                                            # Interface name
arch="x64"                                                  # Can be "x64" or "x86", used for libraries
os="kali"                                                   # Can be "kali" or "custom", if the later is used then the script will get latest libs & snort
snort_dir="/etc/snort/"                                     # snort directory
snort_conf="snort.conf"                                     # snort configuration file
snort_rules="rules/"                                        # snort rules directory 
mysql_root_pass=""                                          # MYSQL root password, used for creating a new database & database user
mysql_snorby_db="snorby"                                    # MYSQL database name for snorby
mysql_snorby_user="snorby"                                  # MYSQL database user for snorby
mysql_snorby_pass="snorby"                                  # MYSQL database password for snorby
snorby_db_config="config/database.yml"                      # Database configuration file
snorby_mail_config="config/initializers/mail_config.rb"     # Mail configuration file
snorby_config="config/snorby_config.yml"                    # Snorby configuration file
script_path="$( cd "$(dirname "$0")" ; pwd)"                # Path to script directory (make sure init script is in the same directory)

#--- Checking for the variables that were set
echo -e "\n\e[01;32m[>]\e[00m Starting the automated SNORT script,"

if [ $arch == "x86" ]; then
    lib_path="i386-linux-gnu"
elif [ $arch == "x64" ]; then
    lib_path="x86_64-linux-gnu"
else 
    echo -e "\n\e[01;31m[!]\e[00m Error detected, no architecture set or wrong architecture. Exiting ...\n"
    exit
fi

if [ -z $mysql_root_pass ]; then
    mysql_command="mysql -uroot"
else 
    mysql_command="mysql -uroot -p$mysql_root_pass"
fi


#--- Installing dependencies 
echo -e "\e[01;32m[>]\e[00m Installing dependencies\n"

apt-get -qq update && apt-get -y --fix-missing -qq install apache2 apache2-doc autoconf automake bison ca-certificates ethtool flex g++ gcc libapache2-mod-php5 libcrypt-ssleay-perl libnet1 libnet1-dev libpcre3 libpcre3-dev libssl-dev libtool libwww-perl make ntp php5-cli php5-gd php5-mysql php-pear sysstat usbmount dh-autoreconf php-pear libpcap0.8 libpcap-dev libdaq-dev libdaq0 apache2 libapr1-dev libaprutil1-dev libopenssl-ruby libcurl4-openssl-dev oinkmaster ruby ruby-dev openjdk-7-jre
DEBIAN_FRONTEND=noninteractive apt-get -y install mysql-server 
apt-get -qq install mysql-client mysql-common libmysqlclient-dev
## In case you don't have exim installed
#apt-get update && apt-get -y -qq install sendmail sendmail-bin
gem install --no-rdoc --no-ri bundler
service mysql start  

#--- Disable ”Large Receive Offload” and ”Generic Receive Offload” on the interface
sed -i "/By default this script does nothing/a \nethtool --offload $interface rx off tx off \ \nethtool -K $interface gso off \ \nethtool -K $interface gro off" /etc/rc.local

#--- Libdnet install
cd /usr/src && wget -q http://libdnet.googlecode.com/files/libdnet-1.12.tgz  
tar xvf libdnet-*.tgz && rm -rf libdnet-*.tgz && cd libdnet-*  
./configure --prefix=/usr --enable-shared && make && make install  

#--- Checking  the $os variable which will determine if we install snort using repos or compile it with the latest libs
if [ $os == "kali" ]; then
    DEBIAN_FRONTEND=noninteractive apt-get -y install snort  
    ln -s /usr/sbin/snort /usr/bin/snort
elif [ $os == "custom" ]; then
    ## Downloading and compiling Libpcap
    cd /usr/src  && wget -q http://www.tcpdump.org/release/libpcap-1.6.1.tar.gz  
    tar xvf libpcap-*.tar.gz && rm -rf libpcap-*.tar.gz && cd libpcap-*  
    ./configure --prefix=/usr && make && make install  

    ## Downloading and compiling Libdaq
    cd /usr/src && wget -q https://www.snort.org/downloads/snort/daq-2.0.4.tar.gz  
    tar xvf daq-*.tar.gz && rm -rf daq-*.tar.gz && cd daq-*  
    ./configure --prefix=/usr && make && make install  

    ## Downloading and compiling Snort
    cd /usr/src/ && wget -q https://www.snort.org/downloads/snort/snort-2.9.7.0.tar.gz 
    tar xvf snort-*.tar.gz && rm -rf snort-*.tar.gz && cd snort-* 
    ./configure --enable-sourcefire && make && make install 
    mkdir /etc/snort/ /etc/snort/rules /var/log/snort /var/log/barnyard2 /usr/local/lib/snort_dynamicrules 
    touch /etc/snort/rules/white_list.rules /etc/snort/rules/black_list.rules /etc/snort/sid-msg.map 
    groupadd snort && useradd -g snort snort 
    chown snort:snort /var/log/snort /var/log/barnyard2
    cp /usr/src/snort-*/etc/*.conf* /etc/snort
    cp /usr/src/snort-*/etc/*.map /etc/snort
    cd /usr/src && wget -q --no-check-certificate https://labs.snort.org/snort/2970/snort.conf 
    cp /usr/src/snort.conf /etc/snort
    rm -rf /usr/src/snort-.tar.gz
    ln -s /usr/local/bin/snort /usr/bin/snort
    sed -i "s/..\/rules/.\/rules/g" $snort_dir$snort_conf
fi

#--- Update shared library path & creating sid-msg.map
echo -e "\n\e[01;32m[>]\e[00m Updating shared library path & creating symbolic link.\n"
echo >> /etc/ld.so.conf /usr/lib
echo >> /etc/ld.so.conf /usr/local/lib && ldconfig
cd /usr/share/oinkmaster && ./create-sidmap.pl /etc/snort/rules > /etc/snort/sid-msg.map

#--- Uncomment if you want latest snort.conf
#cd $snort_config && wget -q --no-check-certificate https://labs.snort.org/snort/2970/snort.conf

#--- Make the required changes to the configuration
echo -e "\n\e[01;32m[>]\e[00m Fixing SNORT configuration."
sed -i "s/ipvar HOME_NET any/ipvar HOME_NET $subnet/g" $snort_dir$snort_conf
#sed -i "s/ipvar EXTERNAL_NET any/ipvar EXTERNAL_NET \!\$HOME_NET/g" $snort_dir$snort_conf # This usually is not needed
sed -i "s/decompress_depth 65535/decompress_depth 65535  max_gzip_mem 104857600/g" $snort_dir$snort_conf
sed -i "s/# unified2/&\noutput unified2: filename snort.log, limit 128/g" $snort_dir$snort_conf
sed -i "s/include \$RULE_PATH/#include \$RULE_PATH/g" $snort_dir$snort_conf
sed -i "s/#include \$RULE_PATH\/local.rules/include \$RULE_PATH\/local.rules/g" $snort_dir$snort_conf

#--- Addng a basic ICMP rule for testing purposes, make sure you remove this after
echo -e "\e[01;32m[>]\e[00m Adding test rule for ICMP traffic.\n"
echo "alert icmp any any -> \$HOME_NET any (msg:\"ICMP test\"; sid:10000001; rev:1;)" >> $snort_dir$snort_rules"local.rules"

#--- Creating database & user to be used by Barnyard2 and Snorby. CHANGE THE PASSWORD!!!!
$mysql_command -e "create database snorby;"  
$mysql_command -e "CREATE USER 'snorby'@'localhost' IDENTIFIED BY 'snorby';" 
$mysql_command -e "grant all privileges on snorby.* to 'snorby'@'localhost' with grant option;" 
$mysql_command -e "flush privileges;" 

#--- Downloading and compiling Barnyard2
cd /usr/src && wget -q https://github.com/firnsy/barnyard2/archive/master.tar.gz 
tar -zxf master.tar.gz && cd barnyard2-* 
autoreconf -fvi -I ./m4 && ./configure --with-mysql --with-mysql-libraries=/usr/lib/$lib_path && make && make install 
mv /usr/local/etc/barnyard2.conf $snort_dir
cd $snort_dir && sed -i "s/output alert_fast: stdout/output alert_fast/g" barnyard2.conf
echo "output database: log, mysql, user=$mysql_snorby_user password=$mysql_snorby_pass dbname=$mysql_snorby_db host=localhost" >> barnyard2.conf
mkdir /var/log/barnyard2/ 
chown snort.snort /var/log/barnyard2

#--- Setting up Snort and Barnyard2 init script
mv -f $script_path"/snort_init.sh" /etc/init.d/snort
chmod +x /etc/init.d/snort

snort_dir_sed=$(sed -e 's/[\/&]/\\&/g' <<< $snort_dir)
snort_conf_sed=$(sed -e 's/[\/&]/\\&/g' <<<"$snort_dir$snort_conf")

sed -i "s/SNORTCONF/$snort_conf_sed/g" /etc/init.d/snort
sed -i "s/INTERFACENR/$interface/g" /etc/init.d/snort
sed -i "s/SNORTDIR /$snort_dir_sed/g" /etc/init.d/snort
insserv -f -v snort 

#--- Installing Snorby dependencies 
echo -e "\n\e[01;32m[>]\e[00m Installing dependencies for Snorby.\n"
apt-get -qq -y install git ruby rails imagemagick wkhtmltopdf libxslt1-dev 

#--- Downloading and installing Snorby
echo -e "\n\e[01;32m[>]\e[00m Downloading and installing Snorby.\n"
cd /var/www/ && git clone http://github.com/Snorby/snorby.git
cd snorby && bundle install 

echo -e "\n\e[01;32m[>]\e[00m Configuring Snorby."
cp -f config/database.yml.example $snorby_db_config
cp -f config/snorby_config.yml.example $snorby_config
awk '{print "#"$0}' $snorby_config > $snorby_mail_config
sed -i "s/username: root/username: $mysql_snorby_user/g" $snorby_db_config
sed -i "s/Enter Password Here/$mysql_snorby_pass/g" $snorby_db_config
sed -i "s/database: snorby/database: $mysql_snorby_db/g" $snorby_db_config

#--- Snorby configuration file
cat <<EOF > $snorby_config
production:
  # in case you want to run snorby under a suburi/suburl under eg. passenger:
  baseuri: ''
  # baseuri: '/snorby'
  domain: 'localhost'
  wkhtmltopdf: /usr/local/bin/wkhtmltopdf
  ssl: false
  mailer_sender: 'admin@localhost'
  #geoip_uri: "http://geolite.maxmind.com/download/geoip/database/GeoLiteCountry/GeoIP.dat.gz"
  rules:
    - "$snort_dir$snort_rules"
  authentication_mode: database
  # If timezone_search is undefined or false, searching based on time will
  # use UTC times (historical behavior). If timezone_search is true
  # searching will use local time.
  timezone_search: true
  # uncomment to set time zone to time zone of box from /usr/share/zoneinfo, e.g. "America/Cancun"
  # time_zone: 'UTC'
EOF

#--- Starting to integrate Snorby with Apache
## If you do not want to use apache you can simply start Snorby: cd /var/www/snorby && bundle exec rails server -e production
echo -e "\e[01;32m[>]\e[00m Integrading Snorby with Apache.\n"
cd /var/www/snorby/ && bundle pack --all
bundle install --path vender/cache 
RAILS_ENV=production bundle exec rake snorby:setup 

#--- Installing Apache mods and enabling SSL
apt-get -y -qq install libapache2-mod-passenger 
a2enmod passenger 
a2enmod ssl 
cp -f /etc/apache2/sites-available/default-ssl /etc/apache2/sites-available/snorby
sed -i "s/\/var\/www/\/var\/www\/snorby\/public/g" /etc/apache2/sites-available/snorby
sed -i "/webmaster@localhost/a \        \ServerName snorby.localhost" /etc/apache2/sites-available/snorby
ln -s /etc/apache2/sites-available/snorby /etc/apache2/sites-enabled/snorby
echo "127.0.0.1     snorby.localhost" >> /etc/hosts
chown www-data:www-data -R /var/www/snorby/

echo -e "\n\e[01;32m[>]\e[00m The script has finished running."
echo -e "\e[01;32m[>]\e[00m Starting apache and the Snort suite."
echo -e "\e[01;32m[>]\e[00m To stop/start it just /etc/init.d/snort stop | start\n"
service apache2 restart
service snort restart
