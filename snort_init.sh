#! /bin/sh
#
### BEGIN INIT INFO
# Provides: snortbarn
# Required-Start: $remote_fs $syslog mysql
# Required-Stop: $remote_fs $syslog
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# X-Interactive: true
# Short-Description: Start Snort and Barnyard
### END INIT INFO
. /lib/init/vars.sh
. /lib/lsb/init-functions

mysqld_get_param() {
	/usr/sbin/mysqld --print-defaults | tr " " "\n" | grep -- "--$1" | tail -n 1 | cut -d= -f2
}

do_start()
{
	log_daemon_msg "Starting Snort and Barnyard" ""
	# Make sure mysql has finished starting
	ps_alive=0
	while [ $ps_alive -lt 1 ];
	do
		pidfile=`mysqld_get_param pid-file`
		if [ -f "$pidfile" ] && ps `cat $pidfile` >/dev/null 2>&1; then 
			ps_alive=1 
		else 
			echo ""
			echo "[....] Could not start service, make sure MYSQL is running" 
			exit 0
		fi
		sleep 1
	done
	snort -q -u snort -g snort -c SNORTCONF -i INTERFACENR &
	/usr/local/bin/barnyard2 -c SNORTDIR barnyard2.conf -d /var/log/snort -f snort.log -w /var/log/snort/bylog.waldo -C SNORTDIR classification.config log_end_msg 0 2> /dev/null &
	log_end_msg 0
	return 0
}

do_stop()
{
	log_daemon_msg "Stopping Snort and Barnyard" ""
	kill $(pidof snort) 2> /dev/null
	kill $(pidof barnyard2) 2> /dev/null
	log_end_msg 0
	return 0
}

case "$1" in
	start)
		do_start
	;;
	stop)
		do_stop
	;;
	restart)
		do_stop
		do_start
	;;
*)

echo "Usage: snort-barn {start|stop|restart}" >&2
exit 3
;;
esac
exit 0